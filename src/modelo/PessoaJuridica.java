package modelo;

public class PessoaJuridica extends Fornecedor {
	
	private String cnpj;
	private String razaoSocial;
	
	public PessoaJuridica(String nome, String telefone,int posicaoArray, String cnpj){
		super(nome,telefone, posicaoArray);
		this.cnpj = cnpj;
	}
	public PessoaJuridica(String nome, String telefone, int posicaoArray){
		super(nome,telefone, posicaoArray);
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	
}

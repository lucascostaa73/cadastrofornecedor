package modelo;

public class PessoaFisica extends Fornecedor{

	private String cpf;
	
	public PessoaFisica(String nome, String telefone,int posicaoArray, String cpf){
		super(nome, telefone, posicaoArray);//Chamar a super classe
		this.cpf = cpf;// Com uma característica própria 
	}
	
	public PessoaFisica(String nome, String telefone, int posicaoArray){
		super(nome, telefone, posicaoArray);//Chamar a super classe
	}
	
	public void setCpf(String cpf){
		this.cpf = cpf;
	}
	public String getCpf() {
		return cpf;
	}
	
	
}

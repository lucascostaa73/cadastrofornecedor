package modelo;
import java.util.ArrayList;

import modelo.Endereco;
import modelo.Produto;

public class Fornecedor {
	
	private int posicaoArray;
	private String nome;
	private String telefone;
	private Endereco endereco;
	private ArrayList<Produto> listaProdutos;
	
	public Fornecedor(String nome, String telefone, int posicaoArray){
		this.nome = nome;
		this.telefone = telefone;
		this.posicaoArray = posicaoArray;
			
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	public void adicionar(Produto umProduto){
		listaProdutos.add(umProduto);
	}
	public void remover(Produto umProduto){
		listaProdutos.remove(umProduto);
	}
	public Produto pesquisar(String umNome){
		for(Produto umProduto : listaProdutos){
			if(umProduto.getNome().equalsIgnoreCase(umNome)) return umProduto;
		}
		return null;
	}
	public String pesquisar(int index){
		return listaProdutos.get(index).getNome();
	}
	
	public void setEndereco(String umaRua,String umBairro,String umNumero ,String umaCidade ,String umEstado ){
		endereco.setRua(umaRua);
		endereco.setCidade(umaCidade);
		endereco.setEstado(umEstado);
		endereco.setBairro(umBairro);
		endereco.setNumero(umNumero);
	}
	public Endereco getEndereco(){
		return endereco;
	}

	public int getPosicaoArray() {
		return posicaoArray;
	}

	public void setPosicaoArray(int posicaoArray) {
		this.posicaoArray = posicaoArray;
	}
	
	
	
}

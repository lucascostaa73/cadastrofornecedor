package visao;
import java.io.*;
import controle.ControleFornecedor;
import modelo.Fornecedor;

public class CadastroFornecedor {
	
	
	public static void main(String[] args) throws IOException{
	    //burocracia para leitura de teclado
	    InputStream entradaSistema = System.in;
	    InputStreamReader leitor = new InputStreamReader(entradaSistema);
	    BufferedReader leitorEntrada = new BufferedReader(leitor);
	    String entradaTeclado;

	    //instanciando objetos lista
	    ControleFornecedor listaFornecedores = new ControleFornecedor();
	    
	    //Bem-vindo � Agenda
	    System.out.println(" ** Controle de Fornecedores ** ");
	    System.out.println("Para sair digite -1");
	    System.out.println("");
	    
	    int pos = 0;
	    
	    for(int count = 0; count<50; count++){ 
	        
	    	pos++;
	        //Varios objetos pessoa precisam ser criado porque cada objeto ter�
	        //um nome diferente e um telefone diferente
	        //Entretanto a Lista ser� apenas uma: Agenda
	        Fornecedor novoFornecedor  = new Fornecedor("","",pos); 
	        
	        //Mensagem de pedido de entrada
	        System.out.println("Digite o nome do Fornecedor:");
	        entradaTeclado = leitorEntrada.readLine();
	        String umNome = entradaTeclado;
	        novoFornecedor.setNome(umNome);
	        if(entradaTeclado.equalsIgnoreCase("-1"))
	            break;
	        
	        System.out.println("Digite o telefone do Fornecedor:");
	        entradaTeclado = leitorEntrada.readLine();
	        String umTelefone = entradaTeclado;
	        novoFornecedor.setTelefone(umTelefone);
	        
	        if(entradaTeclado.equalsIgnoreCase("-1"))
	            break;
	        
	        //adicionando uma pessoa na lista de pessoas do sistema
	        listaFornecedores.adicionar(novoFornecedor);
	    }
	    
	    
	    //Mostrar pessoas cadastradas � lista
	    System.out.println("Pessoas cadastradas:");
	    System.out.println("=================================");
	    
	    	
	    	System.out.println(listaFornecedores.getFornecedor(1).getNome());
	    	System.out.println(listaFornecedores.getFornecedor(2).getNome());
	    
	    System.out.println("=================================");
	    System.out.println("Tenha um Bom dia!");
	    
	    

	  }

}
